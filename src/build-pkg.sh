#!/bin/bash

mkdir -p "usr/share/backgrounds/emilia"

for _images in *.jpg; do
    mkdir -p "wallpapers/${_images%.*}/contents/images"
    
    # Create desktop entry for Plasma.
    echo "[Desktop Entry]
Name=${_images%.*}

X-KDE-PluginInfo-Name=${_images%.*}
X-KDE-PluginInfo-Author=Emilia Team
X-KDE-PluginInfo-Email=
X-KDE-PluginInfo-License=${license}" > "wallpapers/${_images%.*}/metadata.desktop"
        
    # Only obligatory is 1980x1080 because of screenshot below
    for _resolucao in 1980x1080 1366x768; do
        echo "Creating ${_images} in ${_resolucao}..."
        convert "${_images}" -resize ${_resolucao}^ -gravity center -extent ${_resolucao} "wallpapers/${_images%.*}/contents/images/${_resolucao}.jpg"
    done
    cp "wallpapers/${_images%.*}/contents/images/1980x1080.jpg" "wallpapers/${_images%.*}/contents/screenshot.jpg"
    
    # Copy the image to the backgrounds folder.
    cp "${_images}" "usr/share/backgrounds/emilia/"
done

# Put the Plasma wallpapers in place
mv "wallpapers" "usr/share/wallpapers"

#put .xmls in the background folder.
for _markup in *.xml; do
    cp "${_markup}" "usr/share/backgrounds/emilia/"
done
