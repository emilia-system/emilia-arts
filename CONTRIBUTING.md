# Contributing to the Emilia Project

Please, see first the main version of the [Contributing file](https://gitlab.com/emilia-system/extra-files/blob/master/CONTRIBUTING.md). Here are things specific for this project if any is necessary.
