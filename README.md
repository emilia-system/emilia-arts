# Emilia Arts

[![pipeline status](https://gitlab.com/emilia-system/emilia-arts/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-arts/commits/master)
[![License](https://img.shields.io/badge/license-MPL2-orange)](https://choosealicense.com/licenses/mpl-2.0/)

This repository contains all arts used on Emilia for wallpapers used by the system. Utilized along Emilia-config when necessary for the desktop configuration.

## Contributing

We are open to suggestions and are always listening for bugs when possible. For some big change, please start by openning an issue so that it can be discussed.

## Known problems

 - Theses images are PURELY for testing. None of them represents the ideias or final products. Please open an issue if you wish to contact us to draw something for us.

## Licensing

Emilia uses MPL2 for every program created by the team and the original license if based on another program. In this case:

[MPL2](https://choosealicense.com/licenses/mpl-2.0/)

# Emilia Arts - pt_BR

Este repositório contém todas as artes usadas da Emilia para papéis de parede usados pelo sistema. Utilizado junto de Emilia-config quando necessário para as configs de desktops.

## Contribuindo

Somos abertos a sugestões e estamos sempre escutando por bugs quando possível. Para alguma mudança grande, por favor comece abrindo um issue na página para que possa ser discutido.

## Problemas conhecidos

 - Estas imagens são PURAMENTE de teste. Nenhuma delas representa as ideias ou produtos finais. Por favor, abra um issue se deseja nos contatar para desenhar algo para nós.

## Licença

Emilia usa MPL2 para todos os programas criados pelo time e a licença original caso baseado em outro programa. No caso deste:

[MPL2](https://choosealicense.com/licenses/mpl-2.0/)
